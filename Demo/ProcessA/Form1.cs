﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TestDll;

using Yt.Core.IPC;

namespace ProcessA
{
    public partial class Form1 : Form
    {
        IpcProxy Proxy;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string pipename = "yourpipename";

            Proxy = IpcProxy.CreateServer(pipename);
            Proxy.OnConnection = () =>
            {
                log("跨进程通信已经连接");
            };
            Proxy.OnDisconnect = () =>
            {
                log("跨进程通信已经断开");
            };
            Proxy.OnError = (ex) =>
            {
                log("跨进程通信遇到错误:" + ex.Message);
            };
            TestService testService = new TestService();
            testService.ipcProxy = Proxy;

            testService.OnReceived=(info) => { log(info); };
            Proxy.AddService<ITest>(testService);
            Proxy.Connect();

        }
        private void log(string info)
        {
            richTextBox1.AppendText(info + "\n");
        }
    }
}
