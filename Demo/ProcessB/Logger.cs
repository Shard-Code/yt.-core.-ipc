﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessB
{
    public class Logger : TestDll.ILog
    {
        public Action<string> Log { get; set; }

         public void LogInfo(string msg)
        {
            Log?.Invoke(msg);
        }
    }
}
