﻿
namespace Yt.Core.IPC
{
    /// <summary>
    /// 通信消息类
    /// </summary>
    internal class Message
    {
        /// <summary>
        /// 消息类型
        /// </summary>
        public MessageType MessageType { get; set; }
        /// <summary>
        /// 消息内容
        /// </summary>
        public string Data { get; set; }
    }
}
